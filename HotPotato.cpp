#include<iostream>
#include<list>
#include<vector>

using namespace std;

vector<int> hotPotato(int, int);

int main()
{
	int playersInput, movesInput;
	vector<int> order;
	cout << "Enter number of players: ";
	cin >> playersInput;
	cout << "Enter number of moves per round: ";
	cin >> movesInput;
	order = hotPotato(playersInput, movesInput);
	cout << "Eliminate order: ";
	//Declare elimination sequences.
	for(int i = 0; i < playersInput - 1; i++)
	{
		if(i == 0)
		{
			cout << order.at(i);
		}
		else
		{
			cout << " -> " << order.at(i);
		}
	}
	//Declare the winner.
	cout << endl << "The winner is #" << order.at(playersInput - 1) << "!" << endl;
	return 0; //Also, I didn't add anything fancy this time, too busy :(
}

vector<int> hotPotato(int players, int moves)
{
	/*
		How it works:
			1. Make a linked list of players.
			2. Set integer pointer = 1 and iterator ite at the beginning of the linked list.
			3. Pass a pointer for <moves> times. (pointer increases and resets the same as iterator.)
			4. Erase the destination element.
			5. Set iterator back to the head, advance for <pointer> times. This will send iterator back to its original position. (Reason is iterator gets weird after each deletion.)
			6. If the size of <theList> isn't equal to 1, repeat step 3.
			Also, the vector order is for collecting the order of elimination, the last element in the vector is the winner.
	*/
	
	int pointer;
	list<int> theList;
	list<int>::iterator ite;
	vector<int> order;
		//Step 1
	for(int i = 1; i <= players; i++)
	{
		theList.push_back(i);	
	}
		//Step 2
	ite = theList.begin();
	pointer = 1;
		//Step 6 check
	while(theList.size() != 1)
	{
		//Step 3
		for(int i = 1; i <= moves; i++)
		{
			if(pointer >= theList.size())
			{
				ite = theList.begin();
				pointer = 1;
			}
			else
			{
				ite++;
				pointer++;
			}
		}
		//Step 4
		order.push_back(*ite);
		theList.erase(ite);
		//Step 5
		if(pointer > theList.size()) //In case the pointer is out of range after a deletion, then send it to the head.
		{
			pointer = 1;
		}
		ite = theList.begin();
		for(int i = 1; i < pointer; i++)
		{
			ite++;
		}
	}
	order.push_back(*theList.begin());
	return order;
}
